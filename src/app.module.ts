import { Module } from '@nestjs/common';
import { ProductsModule } from './products/products.module';
import { ClientsModule } from './clients/clients.module';
//import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { ProvidersModule } from './providers/providers.module';
import { ProductsVoucherModule } from './products-voucher/products-voucher.module';
import { AuthModule } from './auth/auth.module';
import { CartDetailModule } from './cart_detail/cart_detail.module';
import { RedisCacheModule } from './DB/Redis/redis.module';
import { PostgresModule } from './DB/postgres/postgres.module';

@Module({
  imports: [
    PostgresModule,
    UsersModule,
    ProductsModule,
    ClientsModule,
    ProvidersModule,
    ProductsVoucherModule,
    AuthModule,
    CartDetailModule,
    RedisCacheModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
