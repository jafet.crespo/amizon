import { ApiProperty } from '@nestjs/swagger';
import { Trim } from 'class-sanitizer';
import { Exclude } from 'class-transformer';
import { IsNotEmpty, IsString, MinLength, Matches } from 'class-validator';

const USERNAME_REGEX = /^[a-zA-Z0-9_]+$/;

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(USERNAME_REGEX, {
    message: 'user name can only contains letters, numbers and underscores',
  })
  user_name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(6)
  @Trim()
  user_password: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  user_role: string;
}
