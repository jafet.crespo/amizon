import { Trim } from 'class-sanitizer';
import { IsNotEmpty, IsString, Matches } from 'class-validator';

const USERNAME_REGEX = /^[a-zA-Z0-9_]+$/;

export class FindUserDto {
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(USERNAME_REGEX, {
    message: 'user name can only contains letters, numbers and underscores',
  })
  user_name: string;
}
