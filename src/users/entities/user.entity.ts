import { Exclude } from 'class-transformer';
import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity({ name: 'user_shopping' })
export class User {
  @PrimaryColumn()
  user_name: string;

  @Column()
  @Exclude()
  user_password: string;

  @Column()
  user_role: string;
}
