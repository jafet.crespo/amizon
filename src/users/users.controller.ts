import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  Version,
  Patch,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles-auth.guard';
import { Roles } from 'src/auth/guards/roles.decorator';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create an user' })
  async createUser(@Body() createUserDto: CreateUserDto) {
    return this.usersService.createUser(createUserDto);
  }

  @Get()
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get all users' })
  async findAllUsers() {
    return this.usersService.findAllUsers();
  }

  @Get(':user_name')
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiParam({
    name: 'user_name',
  })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Search an user by its user_name' })
  async findOneUser(@Param('user_name') user_name: string) {
    return this.usersService.findOneUser(user_name);
  }

  @Patch(':user_name/new-password')
  @Version('1')
  //@UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update a user´s password' })
  async updatePassword(
    @Param('user_name') userName: string,
    @Body() userPassword: UpdateUserPasswordDto,
  ) {
    return this.usersService.updatePassword(userName, userPassword);
  }
}
