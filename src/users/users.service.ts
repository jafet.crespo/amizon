import { Injectable, ConflictException, HttpException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { hash } from 'bcrypt';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async createUser(createUserDto: CreateUserDto) {
    const isUser = await this.findOneUser(createUserDto.user_name);
    if (isUser) {
      throw new ConflictException(
        `user with user_name ${createUserDto.user_name} already exist`,
      );
    }

    const hashPassword = await hash(createUserDto.user_password, 10);
    createUserDto = { ...createUserDto, user_password: hashPassword };
    const newUser = this.userRepository.create(createUserDto);

    return await this.userRepository.save(newUser);
  }

  async findAllUsers() {
    return await this.userRepository.find();
  }

  async findOneUser(user_name: string) {
    return await this.userRepository.findOne({
      where: {
        user_name,
      },
    });
  }

  async updatePassword(userName: string, userPassword: UpdateUserPasswordDto) {
    try {
      const newPassword = await hash(userPassword.user_password, 10);
      return await this.userRepository.update(
        { user_name: userName },
        { user_password: newPassword },
      );
    } catch (error) {
      throw new HttpException('Error en la solicitud', 400);
    }
  }
}
