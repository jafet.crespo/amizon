import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { ConflictException } from '@nestjs/common';

describe('UsersService', () => {
  let service: UsersService;
  let userRepository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    userRepository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('createUser', () => {
    const createUserDto: CreateUserDto = {
      user_name: 'testuser',
      user_password: 'testpassword',
      user_role: 'user',
    };

    it('should create a new user', async () => {
      const saveSpy = jest
        .spyOn(userRepository, 'save')
        .mockResolvedValueOnce(createUserDto as any);

      const result = await service.createUser(createUserDto);

      expect(saveSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledWith(createUserDto);
      expect(result).toEqual(createUserDto);
    });

    it('should throw a ConflictException if user already exists', async () => {
      jest
        .spyOn(service, 'findOneUser')
        .mockResolvedValueOnce({ user_name: 'testuser' } as any);

      await expect(service.createUser(createUserDto)).rejects.toThrowError(
        ConflictException,
      );
    });
  });

  describe('findAllUsers', () => {
    it('should return an array of users', async () => {
      const users: User[] = [
        {
          user_name: 'testuser1',
          user_password: 'testpassword1',
          user_role: 'user',
        },
        {
          user_name: 'testuser2',
          user_password: 'testpassword2',
          user_role: 'user',
        },
      ];

      jest.spyOn(userRepository, 'find').mockResolvedValueOnce(users as any);

      const result = await service.findAllUsers();

      expect(result).toEqual(users);
    });
  });

  describe('findOneUser', () => {
    it('should return a user by user_name', async () => {
      const user: User = {
        user_name: 'testuser',
        user_password: 'testpassword',
        user_role: 'user',
      };

      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(user as any);

      const result = await service.findOneUser(user.user_name);

      expect(result).toEqual(user);
    });

    it('should return undefined if user not found', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValueOnce(undefined);

      const result = await service.findOneUser('testuser');

      expect(result).toBeUndefined();
    });
  });
});
