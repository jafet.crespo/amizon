import {
  IsArray,
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsPositive,
  Matches,
  Max,
  Min,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

const PRODUCTPRICEOUT_REGEX = /^[1-9]\d{0,8}(\.\d{1,2})?$/;

export class CreateCartDetailDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  product_id: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  @Min(1)
  @Max(10000000)
  @IsInt()
  quantity_out: number;

  @ApiProperty()
  @IsArray()
  @IsNotEmpty()
  @IsNumberString()
  @Matches(PRODUCTPRICEOUT_REGEX, {
    message:
      'price can contain 10 digits and only two decimal places and be greater than 1',
    each: true,
  })
  price_out: string;
}
