import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateCartDetailDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  client_id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  date: string;
}
