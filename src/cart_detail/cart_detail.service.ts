import { HttpException, Injectable } from '@nestjs/common';
import { CreateCartDetailDto } from './dto/create-cart_detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CartDetail } from './entities/cart_detail.entity';
import { Index, Repository } from 'typeorm';
import { ProductsService } from 'src/products/products.service';
import { ClientsService } from 'src/clients/clients.service';

@Injectable()
export class CartDetailService {
  constructor(
    @InjectRepository(CartDetail)
    private cartDetailRepository: Repository<CartDetail>,
    private productSservice: ProductsService,
  ) {}

  async createCartDetail(cartDetail: CreateCartDetailDto[], client_id: number) {
    const productIds = cartDetail.map((product) => product.product_id);

    const products = await this.productSservice.findProductsByIds(productIds);
    if (products.length !== cartDetail.length)
      throw new HttpException('Product not found', 400);

    const finalQuantities: number[] = [];
    const productsAvailable: number[] = [];
    const newCartDetails: CartDetail[] = [];

    const availables = products.map((product, pos) => {
      const quantity = product.product_quantity - cartDetail[pos].quantity_out;
      let available = false;
      if (quantity >= 0) {
        productsAvailable.push(product.product_id);
        finalQuantities.push(quantity);
        available = true;

        const detail = {
          clientClientId: client_id,
          productProductId: product.product_id,
          quantity_out: cartDetail[pos].quantity_out,
          price_out: parseFloat(cartDetail[pos].price_out),
        };
        newCartDetails.push(this.cartDetailRepository.create(detail));
      }

      return { ...product, price: cartDetail[pos].product_id, available };
    });

    await this.productSservice.updateProductsQuantities(
      productsAvailable,
      finalQuantities,
    );
    await this.cartDetailRepository.insert(newCartDetails);
    return availables;
  }

  findAllCartsDetails() {
    return this.cartDetailRepository.find();
  }

  async findCartDetailByClientId(id: number) {
    const details = await this.cartDetailRepository.find({
      where: { clientClientId: id },
      relations: ['product'],
    });
    return details;
  }
}
