import { Module } from '@nestjs/common';
import { CartDetailService } from './cart_detail.service';
import { CartDetailController } from './cart_detail.controller';
import { ProductsModule } from 'src/products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartDetail } from './entities/cart_detail.entity';
import { ClientsModule } from 'src/clients/clients.module';
import { RedisCacheModule } from 'src/DB/Redis/redis.module';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([CartDetail]),
    ClientsModule,
    ProductsModule,
    RedisCacheModule,
    UsersModule,
  ],
  controllers: [CartDetailController],
  providers: [CartDetailService],
})
export class CartDetailModule {}
