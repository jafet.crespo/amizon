import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Client } from 'src/clients/entities/client.entity';
import { Product } from 'src/products/entities/product.entity';

@Entity({ name: 'cart_detail' })
export class CartDetail {
  @PrimaryColumn({ name: 'client_id' })
  clientClientId: number;

  @ManyToOne(() => Client, (client) => client.cart_Detail)
  @JoinColumn({ name: 'client_id' })
  client: Client;

  @PrimaryColumn({ name: 'product_id' })
  productProductId: number;

  @ManyToOne(() => Product, (product) => product.cartDetail)
  @JoinColumn({ name: 'product_id' })
  product: Product;

  @PrimaryColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  create_date: Date;

  @Column({ default: 'EN ESPERA' })
  cart_state: string;

  @Column()
  quantity_out: number;

  @Column('numeric', { precision: 10, scale: 2 })
  price_out: number;
}
