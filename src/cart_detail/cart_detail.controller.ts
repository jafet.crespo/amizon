import {
  Controller,
  Get,
  Post,
  Body,
  Version,
  UseGuards,
  Req,
  Param,
  ParseIntPipe,
  HttpException,
} from '@nestjs/common';
import { CartDetailService } from './cart_detail.service';
import { CreateCartDetailDto } from './dto/create-cart_detail.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { Roles } from 'src/auth/guards/roles.decorator';
import { RolesGuard } from 'src/auth/guards/roles-auth.guard';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ClientsService } from 'src/clients/clients.service';

@ApiTags('cart-detail')
@Controller('cart-detail')
export class CartDetailController {
  constructor(
    private readonly cartDetailService: CartDetailService,
    private readonly clientService: ClientsService,
  ) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @Version('1')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Creates a cart detail' })
  async createCartDetail(
    @Body() createCartDetailDto: CreateCartDetailDto[],
    @Req() request,
  ) {
    const { user } = request;
    const client = await this.clientService.findClientByUserName(
      user.user_name,
    );
    if (!client) throw new HttpException('Cliente no registrado', 400);
    return this.cartDetailService.createCartDetail(
      createCartDetailDto,
      client.client_id,
    );
  }

  @Get()
  @Version('1')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get all carts details' })
  findAll() {
    return this.cartDetailService.findAllCartsDetails();
  }

  @Get('client/:id')
  @Version('1')
  @ApiBearerAuth()
  @ApiParam({ name: 'client_id' })
  @ApiOperation({ summary: 'get all carts by a client id' })
  findCartDetailByClientId(@Param('id', ParseIntPipe) id: number) {
    return this.cartDetailService.findCartDetailByClientId(id);
  }
}
