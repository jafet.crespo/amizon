import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { CartDetail } from 'src/cart_detail/entities/cart_detail.entity';

@Entity({ name: 'client' })
export class Client {
  @PrimaryGeneratedColumn()
  client_id: number;

  @OneToOne(() => User)
  @JoinColumn({ name: 'user_name' })
  user: User;

  @Column()
  client_name: string;

  @Column()
  email: string;

  @Column()
  direction: string;

  @Column()
  phone: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  update_date: Date;

  @OneToMany(() => CartDetail, (cart_detail) => cart_detail.client)
  cart_Detail: CartDetail[];
}
