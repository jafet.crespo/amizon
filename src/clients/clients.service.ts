import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateClientDto } from './dto/create-client.dto';
import { Client } from './entities/client.entity';
import { UsersService } from 'src/users/users.service';
import { CreateUserClientDto } from './dto/create-user-client.dto';
import { RedisCacheService } from 'src/DB/Redis/redis.service';
import { User } from 'src/users/entities/user.entity';
import { UpdateClientDto } from './dto/update-client.dto';

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(Client)
    private clientRepository: Repository<Client>,
    private readonly usersService: UsersService,
  ) {}

  async createClient(createClientDto: CreateClientDto) {
    const isUser = await this.usersService.findOneUser(
      createClientDto.user_name,
    );
    if (!isUser) {
      throw new NotFoundException(
        `User with user_name ${createClientDto.user_name} not found`,
      );
    }

    const newClient = this.clientRepository.create(createClientDto);
    newClient.user = isUser;
    return await this.clientRepository.save(newClient);
  }

  findAllClients(): Promise<Client[]> {
    return this.clientRepository.find();
  }

  async findClientById(id: number): Promise<Client> {
    return await this.clientRepository.findOne({
      where: {
        client_id: id,
      },
    });
  }

  async findClientByUserName(user_name: string) {
    const findUser = new User();
    findUser.user_name = user_name;
    return await this.clientRepository.findOne({
      where: {
        user: findUser,
      },
    });
  }

  async createClientUser(clientUserDto: CreateUserClientDto) {
    await this.usersService.createUser({
      user_name: clientUserDto.user_name,
      user_password: clientUserDto.user_password,
      user_role: 'client',
    });
    const newClient = await this.createClient({
      user_name: clientUserDto.user_name,
      client_name: clientUserDto.client_name,
      email: clientUserDto.email,
      phone: clientUserDto.phone,
      direction: clientUserDto.direction,
    });

    return newClient;
  }

  async updateClientInfo(id: number, updateInfo: UpdateClientDto) {
    try {
      const { client_name, email, phone, direction } = updateInfo;
      return await this.clientRepository.update(
        { client_id: id },
        {
          client_name,
          email,
          phone,
          direction,
        },
      );
    } catch (error) {
      throw new HttpException('Error al enviar los datos', 400);
    }
  }
}
