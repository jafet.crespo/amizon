import { IsString, IsNotEmpty, Matches, IsEmail } from 'class-validator';
import { Trim } from 'class-sanitizer';
import { ApiProperty } from '@nestjs/swagger';

const NAME_REGEX = /^[a-zA-Z\s]+$/;

export class CreateClientDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  user_name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(NAME_REGEX, {
    message: 'name can only contains letters',
  })
  client_name: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  @Trim()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Trim()
  direction: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  phone: string;
}
