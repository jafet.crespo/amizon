import { PartialType } from '@nestjs/mapped-types';
import { CreateClientDto } from './create-client.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Matches } from 'class-validator';
import { Trim } from 'class-sanitizer';

const NAME_REGEX = /^[a-zA-Z\s]+$/;

export class UpdateClientDto extends PartialType(CreateClientDto) {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(NAME_REGEX, {
    message: 'name can only contains letters',
  })
  client_name?: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  @Trim()
  email?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  direction?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  phone?: string;
}
