import {
  IsString,
  IsNotEmpty,
  Matches,
  IsEmail,
  MinLength,
} from 'class-validator';
import { Trim } from 'class-sanitizer';
import { ApiProperty } from '@nestjs/swagger';

const NAME_REGEX = /^[a-zA-Z\s]+$/;
const USERNAME_REGEX = /^[a-zA-Z0-9_]+$/;

export class CreateUserClientDto {
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(USERNAME_REGEX, {
    message: 'user name can only contains letters, numbers and underscores',
  })
  @ApiProperty()
  user_name: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6)
  @Trim()
  @ApiProperty()
  user_password: string;

  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(NAME_REGEX, {
    message: 'name can only contains letters',
  })
  @ApiProperty()
  client_name: string;

  @IsEmail()
  @IsNotEmpty()
  @Trim()
  @ApiProperty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Trim()
  @ApiProperty()
  direction: string;

  @IsString()
  @IsNotEmpty()
  @Trim()
  @ApiProperty()
  phone: string;
}
