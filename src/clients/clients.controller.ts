import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseIntPipe,
  Version,
  UseGuards,
  Patch,
} from '@nestjs/common';
import { ClientsService } from './clients.service';
import { CreateClientDto } from './dto/create-client.dto';
import { Client } from './entities/client.entity';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { CreateUserClientDto } from './dto/create-user-client.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles-auth.guard';
import { Roles } from 'src/auth/guards/roles.decorator';
import { UpdateClientDto } from './dto/update-client.dto';

@ApiTags('Clients')
@Controller('clients')
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  /*@Post()
  @Version('1')
  async createClient(@Body() createClientDto: CreateClientDto) {
    return this.clientsService.createClient(createClientDto);
  }*/

  @Get()
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get all clients' })
  findAllClients(): Promise<Client[]> {
    return this.clientsService.findAllClients();
  }

  @Get(':id')
  @Version('1')
  @ApiParam({ name: 'client_id' })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get a client by its id' })
  async findClientById(@Param('id', ParseIntPipe) id: number): Promise<Client> {
    return this.clientsService.findClientById(id);
  }

  @Post('register')
  @Version('1')
  @ApiOperation({ summary: 'Create a user-client' })
  async createClientUser(@Body() clientUserDto: CreateUserClientDto) {
    return this.clientsService.createClientUser(clientUserDto);
  }

  @Patch(':id/client-info')
  @Version('1')
  @ApiParam({ name: 'client_id' })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update the information of a client' })
  async updateClientInfo(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateInfo: UpdateClientDto,
  ) {
    return this.clientsService.updateClientInfo(id, updateInfo);
  }
}
