import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RedisCacheService } from 'src/DB/Redis/redis.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector, private cache: RedisCacheService) {}
  async canActivate(context: ExecutionContext) {
    const requiredRoles = this.reflector.get<string[]>(
      'roles',
      context.getHandler(),
    );
    if (!requiredRoles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const userName = request.user.user_name;
    const token = await this.cache.getToken(userName);
    return requiredRoles.includes(token.user_role);
  }
}
