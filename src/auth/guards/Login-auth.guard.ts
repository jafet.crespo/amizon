import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { RedisCacheService } from 'src/DB/Redis/redis.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(private cacheService: RedisCacheService) {}
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const { user_name } = request.params;
    console.log('hola desde el guard');
    //console.log(request);
    console.log(user_name);
    const token = this.cacheService.getToken(user_name);
    return true;
  }
}
