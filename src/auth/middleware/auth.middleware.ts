import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { RedisCacheService } from 'src/DB/Redis/redis.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly cacheService: RedisCacheService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    try {
      const storedToken = await this.cacheService.getToken('user');
      if (!storedToken) {
        throw new Error('Invalid token');
      }
      //const decodedToken = jwt.decode(storedToken);
      //console.log('estoy aqui', decodedToken);
      //req.userID = decodedToken;
      next();
    } catch (error) {
      res.status(401).json({ message: 'Unauthorized' });
    }
  }
}
