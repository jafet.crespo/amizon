import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { ClientsModule } from 'src/clients/clients.module';
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategies/jwt.strategy';
import { RedisCacheModule } from 'src/DB/Redis/redis.module';

@Module({
  imports: [
    ClientsModule,
    UsersModule,
    JwtModule.register({
      secret: 'este es el secreto',
      signOptions: { expiresIn: '20h' },
    }),
    RedisCacheModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [JwtModule],
})
export class AuthModule {}
