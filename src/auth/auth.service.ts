import { Injectable, HttpException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { LoginUserDto } from './dto/login-user.dto';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { RedisCacheService } from 'src/DB/Redis/redis.service';
import { ClientsService } from 'src/clients/clients.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private cacheService: RedisCacheService,
    private clientService: ClientsService,
  ) {}

  async loginUser(loginUserDto: LoginUserDto) {
    const loginUser = await this.usersService.findOneUser(
      loginUserDto.user_name,
    );
    if (!loginUser) {
      throw new HttpException('Invalid Credentials', 403);
    }

    const checkPassword = await compare(
      loginUserDto.user_password,
      loginUser.user_password,
    );
    if (!checkPassword) {
      throw new HttpException('Invalid Credentials', 403);
    }

    /*const checkClient = await this.clientService.findClientByUserName(
      loginUser.user_name,
    );*/
    const payload = {
      user_name: loginUser.user_name,
      //client_id: checkClient.client_id,
    };
    const token = this.jwtService.sign(payload);

    const redisCache = { token: token, user_role: loginUser.user_role };
    this.cacheService.setToken(loginUser.user_name, redisCache);
    return token;
  }
}
