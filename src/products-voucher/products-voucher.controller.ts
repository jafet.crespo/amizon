import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  Version,
  UseGuards,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { ProductsVoucherService } from './products-voucher.service';
import { CreateProductsVoucherDto } from './dto/create-products-voucher.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles-auth.guard';
import { Roles } from 'src/auth/guards/roles.decorator';
import { CreateProductDto } from 'src/products/dto/create-product.dto';

@ApiTags('Products-voucher')
@Controller('products-voucher')
export class ProductsVoucherController {
  constructor(
    private readonly productsVoucherService: ProductsVoucherService,
  ) {}

  @Get()
  @Version('1')
  @ApiBearerAuth()
  findAllVouchers() {
    return this.productsVoucherService.findAllVouchers();
  }

  @Get('provider/:id')
  @Version('1')
  @ApiParam({ name: 'provider_id' })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get all the products of a provider' })
  findProviderProducts(@Param('id', ParseIntPipe) id: number) {
    return this.productsVoucherService.findProviderProducts(id);
  }

  @Post('provider/:id')
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiBearerAuth()
  @ApiParam({ name: 'provider_id' })
  @ApiOperation({ summary: 'Create a voucher for a provider' })
  createVoucherForProvider(
    @Body() voucher: CreateProductsVoucherDto[],
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.productsVoucherService.createVoucher(voucher, id);
  }

  @Post('provider/:id/new-products')
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiBearerAuth()
  @ApiParam({ name: 'provider_id' })
  @ApiOperation({ summary: 'Create a voucher of new products for a provider' })
  newProductsVoucher(
    @Body() products: CreateProductDto[],
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.productsVoucherService.newProductsVoucher(products, id);
  }
}
