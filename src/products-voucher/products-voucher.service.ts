import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductsVoucherDto } from './dto/create-products-voucher.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductsVoucher } from './entities/products-voucher.entity';
import { Repository } from 'typeorm';
import { ProductsService } from 'src/products/products.service';
import { ProvidersService } from 'src/providers/providers.service';
import { CreateProductDto } from 'src/products/dto/create-product.dto';

@Injectable()
export class ProductsVoucherService {
  constructor(
    @InjectRepository(ProductsVoucher)
    private productsVoucherRepository: Repository<ProductsVoucher>,
    private productsService: ProductsService,
    private providersService: ProvidersService,
  ) {}

  async createVoucher(
    voucher: CreateProductsVoucherDto[],
    distributor_id: number,
  ) {
    const productIds = voucher.map((product) => product.product_id);
    const products = await this.productsService.findProductsByIds(productIds);
    if (products.length !== voucher.length)
      throw new HttpException('Product not found', 400);

    const finalQuantities: number[] = [];
    const finalPrices: number[] = [];

    const vouchers = products.map((product, index) => {
      const quantity = product.product_quantity + voucher[index].quantity_in;
      finalQuantities.push(quantity);
      finalPrices.push(parseFloat(voucher[index].price_in));
      return this.productsVoucherRepository.create({
        productProductId: voucher[index].product_id,
        distributorDistributorId: distributor_id,
        price_in: parseFloat(voucher[index].price_in),
        quantity_in: voucher[index].quantity_in,
      });
    });

    await this.productsService.updateProductsQuantities(
      productIds,
      finalQuantities,
    );
    await this.productsService.updateProductsPrices(productIds, finalPrices);
    return await this.productsVoucherRepository.save(vouchers);
  }

  findAllVouchers() {
    return this.productsVoucherRepository.find();
  }

  async findProviderProducts(id: number) {
    return await this.productsVoucherRepository.find({
      where: { distributorDistributorId: id },
      relations: ['product'],
    });
  }

  async newProductsVoucher(products: CreateProductDto[], id: number) {
    const newProducts = await this.productsService.createProduct(products);
    const vouchers = newProducts.map((product) => {
      return this.productsVoucherRepository.create({
        productProductId: product.product_id,
        distributorDistributorId: id,
        quantity_in: product.product_quantity,
        price_in: product.price,
      });
    });
    return await this.productsVoucherRepository.save(vouchers);
  }
}
