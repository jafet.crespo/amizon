import { Module } from '@nestjs/common';
import { ProductsVoucherService } from './products-voucher.service';
import { ProductsVoucherController } from './products-voucher.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsVoucher } from './entities/products-voucher.entity';
import { ProductsModule } from 'src/products/products.module';
import { ProvidersModule } from 'src/providers/providers.module';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProductsVoucher]),
    ProductsModule,
    ProvidersModule,
    UsersModule,
  ],
  controllers: [ProductsVoucherController],
  providers: [ProductsVoucherService],
})
export class ProductsVoucherModule {}
