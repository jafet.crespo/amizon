import { PartialType } from '@nestjs/mapped-types';
import { CreateProductsVoucherDto } from './create-products-voucher.dto';

export class UpdateProductsVoucherDto extends PartialType(
  CreateProductsVoucherDto,
) {}
