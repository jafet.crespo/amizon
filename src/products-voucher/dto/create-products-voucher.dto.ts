import {
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsPositive,
  Matches,
  Max,
  Min,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

const PRODUCTPRICEIN_REGEX = /^[1-9]\d{0,8}(\.\d{1,2})?$/;

export class CreateProductsVoucherDto {
  @ApiProperty()
  @IsInt()
  @IsNotEmpty()
  product_id: number;

  @ApiProperty()
  @IsInt()
  @IsNotEmpty()
  @IsPositive()
  @Min(1)
  @Max(10000000)
  quantity_in: number;

  @ApiProperty()
  @IsNumberString()
  @Matches(PRODUCTPRICEIN_REGEX, {
    message:
      'price can contain 10 digits and only two decimal places and be greater than 1',
  })
  price_in: string;
}
