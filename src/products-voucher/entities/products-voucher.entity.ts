import { Product } from 'src/products/entities/product.entity';
import { Provider } from 'src/providers/entities/provider.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

@Entity({ name: 'products_voucher' })
export class ProductsVoucher {
  @PrimaryColumn({ name: 'product_id' })
  productProductId: number;

  @ManyToOne(() => Product, (product) => product.productsVoucher)
  @JoinColumn({ name: 'product_id' })
  product: Product;

  @PrimaryColumn({ name: 'distributor_id' })
  distributorDistributorId: number;

  @ManyToOne(() => Provider, (provider) => provider.productsVoucher)
  @JoinColumn({ name: 'distributor_id' })
  distributor: Provider;

  @PrimaryColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  create_date: Date;

  @Column()
  quantity_in: number;

  @Column({ nullable: true, type: 'numeric' })
  price_in: number;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  update_date: Date;
}
