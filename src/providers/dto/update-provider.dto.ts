import { PartialType } from '@nestjs/mapped-types';
import { CreateProviderDto } from './create-provider.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Matches } from 'class-validator';
import { Trim } from 'class-sanitizer';

const NAME_REGEX = /^[a-zA-Z_]+$/;

export class UpdateProviderDto extends PartialType(CreateProviderDto) {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(NAME_REGEX, {
    message: 'name can only contains letters and underscores',
  })
  distributor_name?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  direction?: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  phone?: string;
}
