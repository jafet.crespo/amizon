import { IsString, IsNotEmpty, Matches } from 'class-validator';
import { Trim } from 'class-sanitizer';
import { ApiProperty } from '@nestjs/swagger';

const NAME_REGEX = /^[a-zA-Z_]+$/;

export class CreateProviderDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  @Matches(NAME_REGEX, {
    message: 'name can only contains letters and underscores',
  })
  distributor_name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  direction: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @Trim()
  phone: string;
}
