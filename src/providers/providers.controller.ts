import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseIntPipe,
  Version,
  UseGuards,
  Patch,
} from '@nestjs/common';
import { ProvidersService } from './providers.service';
import { CreateProviderDto } from './dto/create-provider.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles-auth.guard';
import { Roles } from 'src/auth/guards/roles.decorator';
import { UpdateProviderDto } from './dto/update-provider.dto';

@ApiTags('Providers')
@Controller('providers')
export class ProvidersController {
  constructor(private readonly providersService: ProvidersService) {}

  @Post()
  @Version('1')
  //@UseGuards(JwtAuthGuard, RolesGuard)
  //@Roles('admin')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create a new provider' })
  createProvider(@Body() createProviderDto: CreateProviderDto) {
    return this.providersService.createProvider(createProviderDto);
  }

  @Get()
  @Version('1')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get all the providers' })
  findAllProviders() {
    return this.providersService.findAllProviders();
  }

  @Get(':id')
  @Version('1')
  @ApiBearerAuth()
  @ApiParam({
    name: 'distributor_id',
  })
  @ApiOperation({ summary: 'Find a provider by its id' })
  findProviderById(@Param('id', ParseIntPipe) id: number) {
    return this.providersService.findProviderById(id);
  }

  @Patch(':id/provider-info')
  @Version('1')
  @ApiBearerAuth()
  @ApiParam({
    name: 'distributor_id',
  })
  @ApiOperation({ summary: 'Update provider´s information' })
  updateProviderInfo(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateInfo: UpdateProviderDto,
  ) {
    return this.providersService.updateProviderInfo(id, updateInfo);
  }
}
