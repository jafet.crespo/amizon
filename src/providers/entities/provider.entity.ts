import { ProductsVoucher } from 'src/products-voucher/entities/products-voucher.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'distributor' })
export class Provider {
  @PrimaryGeneratedColumn()
  distributor_id: number;

  @Column()
  distributor_name: string;

  @Column()
  direction: string;

  @Column()
  phone: string;

  @OneToMany(
    () => ProductsVoucher,
    (productsVoucher) => productsVoucher.distributor,
  )
  productsVoucher: ProductsVoucher[];
}
