import { HttpException, Injectable } from '@nestjs/common';
import { CreateProviderDto } from './dto/create-provider.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Provider } from './entities/provider.entity';
import { Repository } from 'typeorm';
import { UpdateProviderDto } from './dto/update-provider.dto';

@Injectable()
export class ProvidersService {
  constructor(
    @InjectRepository(Provider)
    private providersRepository: Repository<Provider>,
  ) {}

  async createProvider(createProviderDto: CreateProviderDto) {
    const newProvider = this.providersRepository.create(createProviderDto);
    return await this.providersRepository.save(newProvider);
  }

  findAllProviders() {
    return this.providersRepository.find();
  }

  async findProviderById(id: number) {
    return await this.providersRepository.findOne({
      where: { distributor_id: id },
    });
  }

  async updateProviderInfo(id: number, updateInfo: UpdateProviderDto) {
    try {
      const { distributor_name, direction, phone } = updateInfo;
      return this.providersRepository.update(
        { distributor_id: id },
        { distributor_name, direction, phone },
      );
    } catch (error) {
      throw new HttpException('Error en la solicitud', 400);
    }
  }
}
