import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisCacheService implements CacheServiceFunction {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}
  setToken(username: string, token: any): void {
    this.cacheManager.set(username, token);
  }
  async getToken(username: string): Promise<CacheToken> {
    return await this.cacheManager.get(username);
  }

  public async setAllowedRoles(roles: string[]): Promise<void> {
    await this.cacheManager.set('allowedRoles', roles);
  }
}
