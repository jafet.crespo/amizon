import { Module, OnModuleInit, Inject } from '@nestjs/common';
import { CacheModule, CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import * as redisStore from 'cache-manager-ioredis';
import { RedisCacheService } from './redis.service';

@Module({
  imports: [
    CacheModule.registerAsync({
      useFactory: () => {
        return {
          store: redisStore,
          host: 'localhost',
          port: 6379,
          ttl: 60 * 60 * 1000,
        };
      },
    }),
  ],
  providers: [RedisCacheService],
  exports: [RedisCacheModule, RedisCacheService],
})
export class RedisCacheModule implements OnModuleInit {
  constructor(
    @Inject(CACHE_MANAGER) private readonly cache: Cache,
    private readonly redisCacheService: RedisCacheService,
  ) {}
  public async onModuleInit() {
    //const logger = new Logger('Cache');
    //logger.log('Redis cache initialized');
    await this.redisCacheService.setAllowedRoles([
      'admin',
      'guest',
      'test',
      'prueba2',
    ]);
  }
}
