interface CacheServiceFunction {
  setToken(username: string, token: string): void;
  getToken(username: string): Promise<CacheToken>;
}
