import { Trim } from 'class-sanitizer';
import { IsNumberString, IsOptional, Matches } from 'class-validator';

const PRODUCTPRICE_REGEX = /^[1-9]\d{0,8}(\.\d{1,2})?$/;

export class FindProductsDto {
  @IsOptional()
  @IsNumberString()
  @Trim()
  @Matches(PRODUCTPRICE_REGEX, {
    message:
      'price can contain 10 digits and only two decimal places and be greater than 1',
  })
  price?: string;

  @IsOptional()
  @IsNumberString()
  product_quantity?: string;
}
