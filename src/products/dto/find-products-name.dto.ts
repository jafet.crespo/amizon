import { Trim } from 'class-sanitizer';
import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

export class FindProductsNameDto {
  @IsString()
  @IsNotEmpty()
  @Trim()
  keyword: string;
}
