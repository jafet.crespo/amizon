import {
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Matches,
  Max,
  Min,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';
import { Trim } from 'class-sanitizer';

const PRODUCTNAME_REGEX = /^[a-zA-Z0-9_\s]+$/;
const PRODUCTPRICE_REGEX = /^[1-9]\d{0,8}(\.\d{1,2})?$/;

export class UpdateProductDto extends PartialType(CreateProductDto) {
  @ApiProperty()
  @IsString()
  @Trim()
  @IsNotEmpty()
  @Matches(PRODUCTNAME_REGEX, {
    message:
      'product name can only contains letters, numbers, spaces and underscores',
  })
  product_name?: string;

  @ApiProperty()
  @IsString()
  @Trim()
  @IsNotEmpty()
  description?: string;

  @ApiProperty()
  @IsNumberString()
  @Matches(PRODUCTPRICE_REGEX, {
    message:
      'price can contain 10 digits and only two decimal places and be greater than 1',
  })
  @IsNotEmpty()
  price?: string;

  @ApiProperty()
  @IsString()
  @Trim()
  @IsNotEmpty()
  category?: string;

  @ApiProperty()
  @IsString()
  @Trim()
  @IsNotEmpty()
  product_type?: string;

  @ApiProperty()
  @IsInt()
  @Min(-10000000)
  @Max(10000000)
  @IsNotEmpty()
  product_quantity?: number;

  @ApiProperty()
  @IsString()
  @Trim()
  @IsNotEmpty()
  image_product?: string;
}
