import { ProductsVoucher } from 'src/products-voucher/entities/products-voucher.entity';
import { CartDetail } from 'src/cart_detail/entities/cart_detail.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'product' })
export class Product {
  @PrimaryGeneratedColumn()
  product_id: number;

  @Column()
  product_name: string;

  @Column()
  description: string;

  @Column('numeric', { precision: 10, scale: 2 })
  price: number;

  @Column()
  product_quantity: number;

  @Column()
  category: string;

  @Column()
  product_type: string;

  @Column()
  image_product: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  create_date: Date;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  update_date: Date;

  @OneToMany(
    () => ProductsVoucher,
    (productsVoucher) => productsVoucher.product,
  )
  productsVoucher: ProductsVoucher[];

  @OneToMany(() => CartDetail, (cartdetail) => cartdetail.product)
  cartDetail: CartDetail[];
}
