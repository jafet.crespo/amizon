import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProductsService } from './products.service';
import { Product } from './entities/product.entity';
import { CreateProductDto } from './dto/create-product.dto';
import { ProductsVoucher } from 'src/products-voucher/entities/products-voucher.entity';
import { CartDetail } from 'src/cart_detail/entities/cart_detail.entity';

describe('ProductsService', () => {
  let productsService: ProductsService;
  let productsRepository: Repository<Product>;

  const productMock: Product = new Product();
  productMock.product_id = 1;
  productMock.product_name = 'Test Product';
  productMock.description = 'Test Product Description';
  productMock.price = 9.99;
  productMock.product_quantity = 10;
  productMock.category = 'Test Category';
  productMock.product_type = 'Test Type';
  productMock.image_product = 'https://test.com/image.png';
  productMock.productsVoucher = [new ProductsVoucher()];
  productMock.cartDetail = [new CartDetail()];

  const productsMock: Product[] = [productMock];

  const createProductDtoMock: CreateProductDto = {
    product_name: 'New Test Product',
    description: 'New Test Product Description',
    price: '19.99',
    product_quantity: 20,
    category: 'New Test Category',
    product_type: 'New Test Type',
    image_product: 'https://test.com/new_image.png',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forFeature([Product])],
      providers: [
        ProductsService,
        {
          provide: getRepositoryToken(Product),
          useClass: Repository,
        },
      ],
    }).compile();

    productsService = module.get<ProductsService>(ProductsService);
    productsRepository = module.get<Repository<Product>>(
      getRepositoryToken(Product),
    );
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('findAllProducts', () => {
    it('should return an array of products', async () => {
      jest
        .spyOn(productsRepository, 'find')
        .mockResolvedValueOnce(productsMock);

      const result = await productsService.findAllProducts();

      expect(result).toEqual(productsMock);
      expect(productsRepository.find).toHaveBeenCalledTimes(1);
    });
  });

  describe('findProductById', () => {
    it('should return a product with the given id', async () => {
      jest
        .spyOn(productsRepository, 'findOne')
        .mockResolvedValueOnce(productMock);

      const result = await productsService.findProductById(1);

      expect(result).toEqual(productMock);
      expect(productsRepository.findOne).toHaveBeenCalledWith({
        where: { product_id: 1 },
      });
    });

    it('should throw an error if product is not found', async () => {
      jest
        .spyOn(productsRepository, 'findOne')
        .mockResolvedValueOnce(undefined);

      await expect(productsService.findProductById(1)).rejects.toThrow();
    });
  });

  describe('createProduct', () => {
    /*it('should create a new product', async () => {
      jest.spyOn(productsRepository, 'create').mockReturnValueOnce(productMock);
      jest.spyOn(productsRepository, 'save').mockResolvedValueOnce(productMock);

      const result = await productsService.createProduct(createProductDtoMock);

      expect(result).toEqual(productMock);
      expect(productsRepository.create).toHaveBeenCalledWith(
        createProductDtoMock,
      );
      expect(productsRepository.save).toHaveBeenCalledWith(productMock);
    });*/
  });
});
