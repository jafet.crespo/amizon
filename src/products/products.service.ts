import { HttpException, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { ILike, In, Repository } from 'typeorm';
import { UpdateProductDto } from './dto/update-product.dto';
import { FindProductsDto } from './dto/find-products.dto';
import { FindProductsNameDto } from './dto/find-products-name.dto';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}

  findAllProducts() {
    return this.productsRepository.find();
  }

  async findProductById(id: number) {
    return await this.productsRepository.findOne({
      where: { product_id: id },
    });
  }

  async createProduct(createProductDto: CreateProductDto[]) {
    const products = createProductDto.map((product) => {
      const newProduct = this.productsRepository.create({
        product_name: product.product_name,
        description: product.description,
        price: parseFloat(product.price),
        product_quantity: product.product_quantity,
        category: product.category,
        product_type: product.product_type,
        image_product: product.image_product,
      });
      return this.productsRepository.create(newProduct);
    });
    return await this.productsRepository.save(products);
  }

  async updateProductInfo(id: number, updateInfo: UpdateProductDto) {
    try {
      const {
        product_name,
        description,
        category,
        product_type,
        product_quantity,
        image_product,
      } = updateInfo;
      const price = parseFloat(updateInfo.price);
      return await this.productsRepository.update(
        { product_id: id },
        {
          product_name,
          description,
          price,
          category,
          product_type,
          product_quantity,
          image_product,
        },
      );
    } catch (error) {
      throw new HttpException('Error al enviar los datos', 400);
    }
  }

  async findProductByCategory(
    category: string,
    filters: FindProductsDto,
    product_type?: string,
  ) {
    const query: any = { category };

    if (product_type) query.product_type = product_type;

    for (const filter in filters) {
      query[filter] = filters[filter];
    }

    return await this.productsRepository.find({ where: query });
  }

  async findProductByName(searchName: FindProductsNameDto) {
    const { keyword } = searchName;
    const keywordSanitize = keyword.replace(/-/g, ' ');
    return await this.productsRepository.find({
      where: [
        { product_name: ILike(`%${keywordSanitize}%`) },
        { description: ILike(`%${keywordSanitize}%`) },
        { category: ILike(`%${keywordSanitize}%`) },
        { product_type: ILike(`%${keywordSanitize}%`) },
      ],
    });
  }

  async findProductsByIds(ids: number[]) {
    return await this.productsRepository.findBy({ product_id: In(ids) });
  }

  async updateProductsQuantities(id: number[], quantity: number[]) {
    return await this.productsRepository
      .createQueryBuilder()
      .update(Product)
      .set({
        product_quantity: () => `CASE 
        ${id
          .map((id, i) => `WHEN product_id = ${id} THEN ${quantity[i]}`)
          .join('\n')}
        ELSE product_quantity
      END`,
      })
      .where(`product_id IN (${id.join(', ')})`)
      .execute();
  }

  async updateProductsPrices(id: number[], prices: number[]) {
    return await this.productsRepository
      .createQueryBuilder()
      .update(Product)
      .set({
        price: () => `CASE 
        ${id
          .map((id, i) => `WHEN product_id = ${id} THEN ${prices[i]}`)
          .join('\n')}
        ELSE price
      END`,
      })
      .where(`product_id IN (${id.join(', ')})`)
      .execute();
  }
}
