import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Patch,
  Version,
  Query,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { FindProductsDto } from './dto/find-products.dto';
import { FindProductsNameDto } from './dto/find-products-name.dto';

@ApiTags('Products')
@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}
  @Get()
  @Version('1')
  @ApiOperation({ summary: 'Get all products' })
  findAllProducts() {
    return this.productsService.findAllProducts();
  }

  @Get(':id')
  @Version('1')
  @ApiParam({ name: 'product_id' })
  @ApiOperation({ summary: 'Search a product by its id' })
  async findProductById(@Param('id', ParseIntPipe) id: number) {
    return this.productsService.findProductById(id);
  }

  @Post()
  @Version('1')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Create  products' })
  async createProduct(@Body() createProductdto: CreateProductDto[]) {
    return this.productsService.createProduct(createProductdto);
  }

  @Patch(':id/product-info')
  @Version('1')
  @ApiParam({ name: 'id' })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Update a product by its id' })
  async updateProductInfo(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateInfo: UpdateProductDto,
  ) {
    return this.productsService.updateProductInfo(id, updateInfo);
  }

  @Get('categories/:category/:product_type?')
  @Version('1')
  @ApiParam({ name: 'category' })
  @ApiParam({ name: 'product_type', required: false })
  @ApiOperation({
    summary:
      'Find products by its category, type and filters (type and filters are optional)',
  })
  async findProductByCategory(
    @Param('category') category: string,
    @Param('product_type') product_type?: string,
    @Query() filters?: FindProductsDto,
  ) {
    return await this.productsService.findProductByCategory(
      category,
      filters,
      product_type,
    );
  }

  @Get('search/name')
  @Version('1')
  @ApiOperation({ summary: 'find a product by a name filter' })
  async findProductByName(@Query() findProductName: FindProductsNameDto) {
    return this.productsService.findProductByName(findProductName);
  }

  /*@Get('search/id')
  @Version('1')
  @ApiOperation({ summary: 'search products by their ids' })
  async findProductsByIds() {
    const ids = [1,2,3,4,5,6];
    return this.productsService.findProductsByIds(ids);
  }*/
}
