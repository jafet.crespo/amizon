import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const configPostgres: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: 5432,
  username: process.env.POSTGRES_USER,
  password: '12c36sd',
  database: process.env.POSTGRES_DB,
  autoLoadEntities: true,
  entities: [__dirname + '/**/*.entity{.ts,.js}'],
  synchronize: true,
};

export default configPostgres;
